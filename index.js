const fs = require('fs');
const Markov = require('markov-text');
// or
// import Markov from 'markov-strings';

function formatOutput(s) {
  var lastChar = s[s.length-1];
  var firstChar = s[0];
  if (!/[\w]/.test(s) && lastChar !== '.') {
    s += '!';
  } else {
    s = s.slice(1, s.length-1) + '!';
  }
  return `${firstChar.toUpperCase()}${s}`;
}

const defaultPreamble = 'Fellow cultists!';

function generate(options, cb) {
  let preamble = options.preamble || defaultPreamble;
  fs.readFile(options.file, 'utf8', (err, txtBlob) => {
    if (err) throw err;

    if (txtBlob.length === 0) {
      throw `No training text loaded from ${options.file}`;
    }

    const horrorGenerator = new Markov(options); // Setup generator
    horrorGenerator.seed(txtBlob) // Seed chain with "training" text
    const generatedText = horrorGenerator.generate(20) // Set length of the generated output.
    cb(null, `${preamble} ${formatOutput(generatedText)}`);
  });
}

// we're being run directly.
if (require.main === module) {

  const defaultFile = './data/thecall.txt';
  const argv = require('yargs')
    .usage('node index.js --file=./data/thecall.txt --mode=multiple --order=10')
    .alias('f', 'file')
    .alias('m', 'mode')
    .alias('o', 'order')
    .default('file', './data/thecall.txt')
    .default('mode', 'multiple')
    .default('order', 10)
    .argv;

  const options = {};
  options.file = argv.file;
  options.mode = argv.mode;
  options.order = argv.order;

  generate(options, (err, result) => {
    console.log(result);
  });

}

exports.generate = generate;
exports.formatOutput = formatOutput;
