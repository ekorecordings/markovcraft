const generate = require('../index').generate;
const formatOutput = require('../index').formatOutput;
const fs = require('fs');

test('test we get some text back', (done) => {
  let options = {
    file: './data/thecall.txt'
  };
  generate(options, (err, result) => {
    if (err) throw err;
    // text output is unpredictable, we just want some text...
    expect(result.length).not.toBe(0);

    // test the formatting we do on the output.
    expect(result).toMatch(/^Fellow cultists! /);
    expect(result.endsWith('.')).toBeFalsy();
    done();
  });
});

test('test formatOutput utility function', () => {
  let s = 'this is some sentence. and another one';
  let result = formatOutput(s);
  expect(result[0]).toEqual('T');
  expect(result.endsWith('!')).toBeTruthy();
});

test('test preamble customization', (done) => {
  let options = {
    file: './data/thecall.txt',
    preamble: 'Dearest friends - '
  };
  generate(options, (err, result) => {
    if (err) throw err;
    // text output is unpredictable, we just want some text...
    expect(result.length).not.toBe(0);

    // test the formatting we do on the output.
    expect(result).toMatch(/^Dearest friends - /);
    done();
  });
});
